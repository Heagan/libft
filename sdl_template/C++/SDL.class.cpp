# include "SDL.class.hpp"

SDLHandler::SDLHandler( int width, int height, std::string name ) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		throw "Failed to init SDL!";
	}

	if (!(this->window = SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
	width, height, SDL_WINDOW_SHOWN))) {
		throw "Failed to create window!";
	}

	if (!(this->renderer = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED))) {
		throw "SDL: Failed to create window render!";
	}

	this->width = width;
	this->height = height;
}

SDLHandler::~SDLHandler( void ) {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void	SDLHandler::display( void ) {
	handleInput();
	SDL_RenderPresent(renderer);
}

void	SDLHandler::putpixel(unsigned int x, unsigned int y, unsigned int colour) {
	SDL_SetRenderDrawColor(renderer, colour >> 16, (colour >> 8) % 256, colour % 256, 255);
	SDL_RenderDrawPoint(renderer, x, y);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
}

void	SDLHandler::drawLine(int x1, int y1, int x2, int y2, int colour) {
	SDL_SetRenderDrawColor(renderer, colour >> 16, (colour >> 8) % 256, colour % 256, 255);
	SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
}

void	SDLHandler::fill(int colour) {
	for (int y = 0; y < height ; y++) {
		for (int x = 0; x < width ; x++) {
			putpixel(x, y, colour);
		}
	}
}

void 	SDLHandler::rect(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, unsigned int colour) {
	for (unsigned int y = y1; y <= y2; y++) {
    	for (unsigned int x = x1; x <= x2; x++) {
			putpixel(x, y, colour);
		}
	}
}

void	SDLHandler::clearpixels( void ) {
	SDL_RenderClear(renderer);
}

int  	SDLHandler::handleInput( void ) {
	int	keyCode = 0;

	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT) {
			exit(0);
		}
		if (event.key.state) {
			return keyCode;
		}
		keyCode = event.key.keysym.sym;
		if (keyCode == SDLK_ESCAPE) {
			exit(0);
		}
		pressed.push_back(SDLK_UP);
	}
	return keyCode;
}

void	SDLHandler::setWinName(std::string name) {
	SDL_SetWindowTitle(window, name.c_str());
}
