from draw import *
from math import cos, sin
from random import random, randint

init('COS', (800, 600))


def go():
    for x in range(800):
        for y in range(600):
            ans = random()
            for i in range(1):
                mul = randint(0, 255)
                c = (cos(ans) * mul, sin(ans) * mul,
                     (cos(ans) + sin(ans)) * mul)
                putpixel(x, y, c)


while True:
    update()
    go()
