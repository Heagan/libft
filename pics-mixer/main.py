'''
PLANNING

Goal: Take image and use lots of other images to make that image like a collague

./pmake <image> <directory> <detail level>

500px 500px
if dl = 10
then every 10px 10px is used for average then an image 10% is used 
so each image is stretched/trimmed to 50px 50px
leaves us with an image of 5000px 5000px

- Get all images in directory add to list
- Process them in a class
	- Only work with square size, leave out offset
	- Trim/stretch image to dl%
	- Calculate the colour average of image
- Add object to array

- for each dl of image, get average
- get closest average from another image in list, save index to array
- go through array and create a new image by adding images into one image 
- save image

'''

import sys
import numpy as np
from PIL import Image

from colour import getcol, mkcol, mkcolt

DIRECTORY = '/Users/gsferopo/Desktop/Pictures'
DL = 10

DL_IMG = 0

IMAGES = []
BEST_IMAGES = []
FINAL_IMAGE = None
FINAL_IMAGE_PIX = []


def add_image(index):
	pass


def get_block(pix, x1, y1, x2, y2):
	global FINAL_IMAGE, FINAL_IMAGE_PIX, IMAGES, DL_IMG, BEST_IMAGES
	
	# Get section average
	pc = 0
	for y in range(y1, y2):
		for x in range(x1, x2):
			pc += mkcolt(pix[x, y])

	avg = pc / (DL * DL * mkcol(255, 255, 255))
	thresh = round( mkcol(255, 255, 255) * avg / 1.15) 

	average = getcol(thresh)
	
	# Get best image that has a similar average
	# best_average = 0
	# best_average_index = 0
	# for i, p in enumerate(IMAGES):
	# 	if abs(p.getAverage() - thresh) < best_average:
	# 		best_average_index = i
	# 		best_average = abs(p.getAverage() - thresh)
	# # Get image data and save to array
	# best_image = IMAGES[best_average_index]
	# pix = best_image.get_pixels()
	# print('Last Step')
	for x in range(DL_IMG):
		for y in range(DL_IMG):
			FINAL_IMAGE_PIX[x, y] = mkcol(255, 255, 255)#pix[x, y]
	# print('Done')

def process(im):
	global FINAL_IMAGE, FINAL_IMAGE_PIX, DL_IMG


	

	try:
		im = Image.open(im)
	except Exception as e:
		print("Unable to open file: " + im)
		print(e)
		print("Make sure to include picture extension, eg 'example.png', 'example.jpg'")
		return

	
	im = im.convert('RGB')
	pix = im.load()

	# Get image size for processing
	n = im.width if im.width < im.height else im.height
	DL_IMG = int(n * (DL / 100))

	FINAL_IMAGE = Image.new('RGB', (im.width * DL, im.height * DL))
	FINAL_IMAGE_PIX = FINAL_IMAGE.load()

	bx = 0
	by = 0
	y = 0
	while y < (im.height):
		x = 0
		while x < (im.width):
			if x % DL == 0:
				get_block(pix, bx, by, x, y)
				bx = x
			if y % DL == 0:
				get_block(pix, bx, by, x, y)
				by = y
			x += 1
			print('\r\t\t', x, '/', im.width, y, '/', im.height, end='')
		y += 1

	FINAL_IMAGE.save('image.png', format='png')




 
if __name__ == '__main__':
	if len(sys.argv) != 2:
		FILE = input("Enter filename of picture: ")
	else:
		FILE = sys.argv[1]
	process(FILE)



