import sys
import sdl2
import sdl2.ext

screen_width = 0
screen_height = 0

window = None
surface = None
pixels = None

def clear():
    global surface, screen_width, screen_height
    sdl2.ext.fill(surface, (0, 0, 0), (0, 0, screen_height, screen_width))

def draw_line(x1, y1, x2, y2, colour):
    global surface
    if x1 >= screen_width or x1 < 0:
        return
    if y1 >= screen_height or y1 < 0:
        return
    if x2 >= screen_width or x2 < 0:
        return
    if y2 >= screen_height or y2 < 0:
        return
    sdl2.ext.line(surface, colour, (int(x1), int(y1), int(x2), int(y2)))

def putpixel(y, x, colour=(255, 255, 255)):
    global surface, screen_width, screen_height, pixels
    if surface is None:
        return
    x = int(x)
    y = int(y)
    if y >= screen_height or y < 0:
        return
    if x >= screen_width or x < 0:
        return
    pixels[y][x] = max(0, min(255, colour[0])) * 256 * 256 + \
                    max(0, min(255, colour[1])) * 256 + \
                    max(0, min(255, colour[2]))


def init(title, size=(800, 600)):
    global window, surface, screen_width, screen_height, pixels
    sdl2.ext.init()
    window = sdl2.ext.Window(title, size=size)
    window.show()
    
    surface = window.get_surface()
    pixels = sdl2.ext.pixels2d(surface)

    screen_width = size[1]
    screen_height = size[0]

def rect(x, y, w, h, c=(255,255,255)):
    for x1 in range(x, x + w):
        for y1 in range(y, y + h):
            putpixel(x1, y1, c)

def update(delay=0, clear_screen=False):
    global window
    if clear_screen:
        clear()
    events = sdl2.ext.get_events()
    for event in events:
        if event.type == sdl2.SDL_QUIT:
            exit()
    window.refresh()
    sdl2.SDL_Delay(delay)