from numpy import complex, array 
from draw import *
import colorsys 


SW = 800
SH = 600

init('Fractol', (SW, SH))

# a function to return a tuple of colors 
# as integer value of rgb 
def rgb_conv(i): 
    color = 255 * array(colorsys.hsv_to_rgb(i / 255.0, 1.0, 0.5)) 
    return tuple(color.astype(int)) 
  
# function defining a mandelbrot 
def mandelbrot(x, y): 
    c0 = complex(x, y) 
    c = 0
    for i in range(1, 1000): 
        if abs(c) > 2: 
            return rgb_conv(i) 
        c = c * c + c0 
    return (0, 0, 0) 
  
  
for x in range(SW): 
    # displaying the progress as percentage 
    for y in range(SH):
        colour = mandelbrot((x - (0.75 * SW)) / (SW / 4), 
                                      (y - (SW / 4)) / (SW / 4)) 
        putpixel(x, y, colour)
  
while True:
    update()