from draw import *

SW = 800
SH = 600

WHITE = (255, 255, 255)

def scale(val, src, dst):
    return ((val - src[0]) / (src[1]-src[0])) * (dst[1]-dst[0]) + dst[0]

def main():
    init("Fractol", (SW, SH))
    update()

    ix = 0
    iy = 0
    while True:
        update()
        for py in range(0, SH):
            for px in range(0, SW):

                # if px == py:
                #     putpixel(x=px, y=py, colour=(0, 0, 255))
                # continue


                x0 = scale(px, (0, SW), (-2.5 + ix, 1 - ix))
                y0 = scale(py, (0, SH), (-1 + iy, 1 - iy)) 

                x = 0
                y = 0
                
                iteration = 0
                max_iteration = 1000
                while (x*x + y*y <= 4 and iteration < max_iteration):
                    xtemp = x*x - y*y + x0
                    y = 2*x*y + y0
                    x = xtemp
                    iteration += 1
                
                c = scale(iteration, (0, max_iteration), (0, 255 + 255 + 255))

                r = max(0, min(c, 255))
                g = max(0, min(c - 255, 255))
                b = max(0, min(c - 255 - 255, 255))

                putpixel(px, py, (r, g, b))
        ix += 1
        iy += 1

if __name__ == '__main__':
    main()